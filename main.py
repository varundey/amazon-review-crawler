from flask import Flask, render_template, request
from selenium import webdriver as wd

review_file = open("reviews.txt", 'w')
specs_file = open("specs.txt", 'w')

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/', methods=['POST'])
def show_reviews():
    product_id = request.form['id']
    review_string = ""
    page = 1
    driver = wd.Firefox()
    while True:
        url = "http://www.amazon.in/product-reviews/" + product_id + "/ref=cm_cr_getr_d_paging_btm_"\
              + str(page) + "?pageNumber=" +str(page)
        print url
        driver.get(url)
        review_obj = driver.find_elements_by_class_name("review-text")
        print review_obj
        if len(review_obj) == 0:
            break
        for review in review_obj:
            review_file.write(review.text.encode("utf-8"))
            review_file.write('\n' + "-------------------------" + '\n')
            review_string += review.text
        page += 1

    url = "http://www.amazon.in/dp/" + product_id
    print url
    driver.get(url)
    specs_table = driver.find_element_by_class_name("pdTab")
    specs_label = specs_table.find_elements_by_class_name("label")
    specs_value = specs_table.find_elements_by_class_name("value")

    for label, value in zip(specs_label, specs_value):
        specs_file.write(label.text + ":" + value.text + '\n')

    driver.quit()
    request.environ.get('werkzeug.server.shutdown')
    return "All done. Check reviews.txt for scraped reviews and specs.txt for scraped specifications"

if __name__ == '__main__':
    app.run(debug=True)
