# Amazon Review Crawler
## How to run

1. ```$ git clone https://varundey@bitbucket.org/varundey/amazon-review-crawler.git```
2. ```$ cd amazon-review-crawler/```
3. ```$ pip install -r requirements.txt```
4. ```$ python main.py```
5. Open `127.0.0.1:5000`
6. Enter the product ID and click submit.
7. Reviews of the product will be written in the file `reviews.txt`

## How to find the ID of the product
 - For the product `http://www.amazon.in/Amazon-Stick-Remote-Streaming-Player/dp/B01EU2M62S/ref=cm_cr_arp_d_product_top?ie=UTF8`, the ID is `B01EU2M62S`
 - For the product `http://www.amazon.in/All-New-Kindle-E-reader-Glare-Free-Touchscreen/dp/B0186FF45G/ref=pd_rhf_dp_p_img_1?_encoding=UTF8&psc=1&refRID=1AE8WH27G19J3J0MXA70`, the product id is `B0186FF45G`